Feature: Trace Route functionality of Google Maps
	As a user
	I want to search the address related to the place
	So that I can share a Route from another place
	
Scenario: Search by Teatro Amazonas
	Given I open the Google Maps app
	When I input 'Teatro Amazonas' 
	And I perform Search option
	Then the app should return the related address
	
Scenario: Map route between Shopping and Teatro Amazonas
	Given I find the 'Teatro Amazonas' address
	When I select Route option
	And I input Start Place 'Shopping Amazonas'
	Then the app should show route map
	
Scenario: Send route to smartphone 
	Given I have the route map
	When I select Sharing Route option
	And I select phone number 
	And I perform Send message 
	Then the app should send Route 