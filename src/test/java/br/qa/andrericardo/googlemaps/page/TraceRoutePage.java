package br.qa.andrericardo.googlemaps.page;

import static br.qa.andrericardo.googlemaps.core.DriverFactory.getDriver;

import java.net.MalformedURLException;

import org.openqa.selenium.By;

import br.qa.andrericardo.googlemaps.core.BasePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;


public class TraceRoutePage extends BasePage{
	
	public void filterPlace(String placeName, String filterName) throws MalformedURLException {
		
		searchPlace(placeName);
		getDriver().findElement(MobileBy.AccessibilityId(filterName)).click();
	}
	
	public void clickRouteOption(String option) {
	
		clickGeneralOption(option);
	}

	public void findStepsStartOn(String fromPlace) throws MalformedURLException {
		
		clickOnText("Seu local");
		clickOnText("Escolher o ponto de partida");
		touchAndInput(fromPlace);
		pressSomeKey(AndroidKeyCode.KEYCODE_ENTER);	
	}
	
	public void scrollEndPage() {
		
		scroll(0.9, 0.1);
		scroll(0.7, 0.1);
	}
	
	public void inputFoneNumber(String foneNumberValue) throws MalformedURLException {
		
		MobileElement foneNumberField = getDriver().findElementById("com.google.android.apps.maps:id/sendkit_ui_autocomplete_text_view");
		foneNumberField.sendKeys(foneNumberValue);
		foneNumberField.click();
		pressSomeKey(AndroidKeyCode.KEYCODE_ENTER);
	}
	
	public void pressOKButton() throws MalformedURLException {
		
		tapCoordinatePoint(0.90, 0.95);
	}
	
	public void pressSendButton() throws MalformedURLException{
		
		pressSomeKey(AndroidKeyCode.BACK);
		tapCoordinatePoint(0.83, 0.90);
	}
	
	public void shareRoute() {
		
		getDriver().findElement(By.xpath("//*[@text='Compartilhar rotas']")).click();
	}
}
