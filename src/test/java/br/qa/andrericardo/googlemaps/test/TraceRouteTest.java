package br.qa.andrericardo.googlemaps.test;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.Test;

import br.qa.andrericardo.googlemaps.core.BasePage;
import br.qa.andrericardo.googlemaps.core.BaseTest;
import br.qa.andrericardo.googlemaps.page.TraceRoutePage;

/**
 * @author Andr Ricardo
 * 
 * QA Engineer Challenge (Google Maps)
 * This test project is designed to solve the challenge proposed 
 * to assess skills to the intended QA Engineer position. 
 * 
 */
public class TraceRouteTest extends BaseTest{
	
	private TraceRoutePage trace = new TraceRoutePage();
	private BasePage page = new BasePage();
	
	@Test
	public void generalSearch() throws MalformedURLException {
		
		page.searchPlace("Teatro Amazonas");
		Assert.assertTrue(page.foundContent("Teatro Amazonas"));	
	}
	
	@Test
	public void specificSearch() throws MalformedURLException {
		
		String address = "Largo de São Sebastião - Centro, Manaus - AM, 69067-080";
		trace.filterPlace("teatro Amazonas", "Teatro Amazonas");
		delay(1500);
		Assert.assertTrue(page.textOccurrenceCount(address));
	}

	@Test
	public void mapRoute() throws MalformedURLException {

		trace.filterPlace("teatro Amazonas", "Teatro Amazonas");
		trace.clickRouteOption("Rotas");
		trace.findStepsStartOn("Amazonas Shopping");
		
		page.clickOnText("Etapas e mais");
		Assert.assertTrue(page.textOccurrenceCount("Etapas"));
		Assert.assertTrue(page.textOccurrenceCount("Amazonas Shopping"));
		
		trace.scrollEndPage();
		Assert.assertTrue(page.textOccurrenceCount("Teatro Amazonas"));
		Assert.assertTrue(page.textOccurrenceCount("O destino estar  direita"));
	}
	
	@Test
	public void sendRoute() throws MalformedURLException {
		
		trace.filterPlace("teatro Amazonas", "Teatro Amazonas");
		trace.clickRouteOption("Rotas");
		trace.findStepsStartOn("Amazonas Shopping");
		
		page.tapDropDownMenu();
		trace.shareRoute();
		trace.inputFoneNumber("(92) 99999-9999");
		trace.pressOKButton();
		trace.pressSendButton();
		page.clickENVMsg();
		
		Assert.assertTrue(page.textOccurrenceCount("MMS"));
		Assert.assertTrue(page.textOccurrenceCount("VER TUDO"));
		Assert.assertTrue(trace.existScreenOption("Anexar"));
		Assert.assertTrue(trace.existScreenOption("Chamar"));	
	}
	
}