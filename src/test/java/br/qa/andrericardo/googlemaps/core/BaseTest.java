package br.qa.andrericardo.googlemaps.core;

import static br.qa.andrericardo.googlemaps.core.DriverFactory.getDriver;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.TouchAction;

public class BaseTest {
	
	@Rule
	public TestName testName = new TestName();
	
	@AfterClass
	public static void finishClass() {
		
		DriverFactory.killDriver();
	}
	
	@After
	public void tearDown() {
		 
		gerarScreenShot();
	}
	
	public void gerarScreenShot() {
		
		File image = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(image, new File("target/screenshots/"+ testName.getMethodName() +".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void delay(long t) {
		
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
