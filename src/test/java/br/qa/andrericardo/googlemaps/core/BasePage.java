package br.qa.andrericardo.googlemaps.core;

import static br.qa.andrericardo.googlemaps.core.DriverFactory.getDriver;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.touch.offset.PointOption;

public class BasePage {
	
	public String getTextResult(By by) {
		
		return getDriver().findElement(by).getText();
	}

	public void searchPlace(String placeName) throws MalformedURLException {
		
		tapCoordinatePoint(0.24, 0.09);
		touchAndInput(placeName);	    
	    pressSomeKey(AndroidKeyCode.KEYCODE_ENTER);
	}
	
	public void touchAndInput(String place) {
		
		MobileElement element = (MobileElement) getDriver().findElementById("com.google.android.apps.maps:id/search_omnibox_edit_text");
		element.click();
		element.sendKeys(place);
	}
	
	public void pressSomeKey(int key) {
		
		getDriver().pressKeyCode(key);
	}
	
	public boolean foundContent(String text) {
		
		String content = getTextResult(MobileBy.AccessibilityId(text));
		
		return (content.length() > 0) ? true:false;
	}
	
	public boolean textOccurrenceCount(String text) {
		
		List<MobileElement> elements = getDriver().findElements(By.xpath("//*[@text='"+text+"']"));
		return elements.size() > 0;
	}
	
	public void clickGeneralOption(String option) {
		
		// Options: Rotas, Ligar, Salvar, Compatilhar 
		getDriver().findElement(By.xpath("//android.widget.TextView[@content-desc='"+option+"']")).click();
		
	}
	
	public void clickOnText(String text) {
		
		getDriver().findElement(By.xpath("//*[@text='"+text+"']")).click();
	}
	
	public void scroll(double vert_begin, double vert_end) {
		
		Dimension size = getDriver().manage().window().getSize();
		
		int x = size.width / 2;
		int start_y = (int) (size.height * vert_begin);
		int end_y = (int) (size.height * vert_end);
		
		TouchAction touchAction = new TouchAction(getDriver());
	    touchAction.longPress(PointOption.point(x, start_y)).
	    	moveTo(PointOption.point(x, end_y)).
	    	release().perform();
	}
	
	public void tapDropDownMenu() {

		getDriver().findElement(By.xpath("//android.widget.ImageView[@content-desc='Menu flutuante']")).click();
	}
	
	public boolean existScreenOption(String screenOption) {
		
		//List empty -> Element Dont Exist | List not empty -> Element exist
		return !(getDriver().findElementsByAccessibilityId(screenOption).isEmpty());
	}
	
	//proportional coordinates tap on
	public void tapCoordinatePoint(double ref_x, double ref_y) throws MalformedURLException{
		
		Dimension size = getDriver().manage().window().getSize();
		
		int x = (int) (size.width * ref_x);
		int y = (int) (size.height * ref_y);
		
		TouchAction touchAction = new TouchAction(getDriver());
		touchAction.tap(new PointOption().withCoordinates(x, y)).perform();
	}
	
	public void clickENVMsg() {
		
		getDriver().findElement(By.id("com.samsung.android.messaging:id/send_button")).click();
	}
}
